#!/bin/sh

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

[ -f ${HOME}/.aliases ] && . ${HOME}/.aliases

