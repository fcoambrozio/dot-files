#!/bin/sh

if [ "$USER" != "root" ]; then
    mkdir -p /dev/shm/${USER}
    (
    cd $HOME && ln -sf /dev/shm/${USER} .cache
    )
fi

if [ "$USER" = "root" ]; then
    PS1="\[\033[0;31m\]\u@\h:\w#\[\033[0m\] "
fi

[ -d ${HOME}/bin ] && PATH="${PATH}:${HOME}/bin"

